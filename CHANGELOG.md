## [1.0.23](https://gitlab.com/msclock/univesal/compare/v1.0.22...v1.0.23) (2024-01-27)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.6.6 ([c1badd9](https://gitlab.com/msclock/univesal/commit/c1badd92088d2923a57c7493d9310a885ca66529))


### CI

* change npm source ([4181228](https://gitlab.com/msclock/univesal/commit/41812288ac661552a58f6396ed441cf4f90da57e))
* speed up unicode_bidi_test ([061e033](https://gitlab.com/msclock/univesal/commit/061e03329671b2eda81fe96d044a02b8352f9bd3))

## [1.0.22](https://gitlab.com/msclock/univesal/compare/v1.0.21...v1.0.22) (2023-12-11)


### Dependencies

* **deps:** update pre-commit hook igorshubovych/markdownlint-cli to v0.38.0 ([d6e1bb4](https://gitlab.com/msclock/univesal/commit/d6e1bb4b1d96d73419602570ddc286a6d9d27db5))


### Chores

* Add LICENSE ([f520b67](https://gitlab.com/msclock/univesal/commit/f520b676d92069970b736f5d8038224f76872778))

## [1.0.21](https://gitlab.com/msclock/univesal/compare/v1.0.20...v1.0.21) (2023-12-05)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.53 ([d693edf](https://gitlab.com/msclock/univesal/commit/d693edf8e169637e2bf5b6f21bd299ef4082aba5))

## [1.0.20](https://gitlab.com/msclock/univesal/compare/v1.0.19...v1.0.20) (2023-11-29)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.45 ([b8ba3a1](https://gitlab.com/msclock/univesal/commit/b8ba3a11011e62ee8eb45d1c529cbeb59d87431f))

## [1.0.19](https://gitlab.com/msclock/univesal/compare/v1.0.18...v1.0.19) (2023-11-28)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.44 ([3ebac85](https://gitlab.com/msclock/univesal/commit/3ebac85c887978dc8925abe860513d48d26d66e7))

## [1.0.18](https://gitlab.com/msclock/univesal/compare/v1.0.17...v1.0.18) (2023-11-27)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.42 ([d776da1](https://gitlab.com/msclock/univesal/commit/d776da1f07dbad21ac3caf7f7f413c4ffcb6e0f6))


### CI

* change the github mirror ([e44d679](https://gitlab.com/msclock/univesal/commit/e44d6793531d3e77cc4fc86566b4af8416a1e566))

## [1.0.17](https://gitlab.com/msclock/univesal/compare/v1.0.16...v1.0.17) (2023-11-24)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.35 ([e4c7e2c](https://gitlab.com/msclock/univesal/commit/e4c7e2c223353059feece8b384d9f050f1b7d118))

## [1.0.16](https://gitlab.com/msclock/univesal/compare/v1.0.15...v1.0.16) (2023-11-20)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.32 ([77c83f0](https://gitlab.com/msclock/univesal/commit/77c83f07fba80fa7afc87cea8148f3d5d3873e79))

## [1.0.15](https://gitlab.com/msclock/univesal/compare/v1.0.14...v1.0.15) (2023-11-12)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.31 ([0d5eb80](https://gitlab.com/msclock/univesal/commit/0d5eb803cac5e80caa4d50aef78d7401f6632a9f))

## [1.0.14](https://gitlab.com/msclock/univesal/compare/v1.0.13...v1.0.14) (2023-11-08)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.30 ([f3cfa97](https://gitlab.com/msclock/univesal/commit/f3cfa9737a296aea738c15167e9075f831e8520a))

## [1.0.13](https://gitlab.com/msclock/univesal/compare/v1.0.12...v1.0.13) (2023-11-02)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.29 ([c553eb5](https://gitlab.com/msclock/univesal/commit/c553eb52fa54c1fcb82fbe4ecbd1ba92a257898b))

## [1.0.12](https://gitlab.com/msclock/univesal/compare/v1.0.11...v1.0.12) (2023-11-02)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.27 ([9898186](https://gitlab.com/msclock/univesal/commit/98981861a85b92a302293090b16ad29fbcd9364a))

## [1.0.11](https://gitlab.com/msclock/univesal/compare/v1.0.10...v1.0.11) (2023-10-22)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.23 ([63b52fc](https://gitlab.com/msclock/univesal/commit/63b52fc292959e0ecf18fa7e6a26096103d59d3d))

## [1.0.10](https://gitlab.com/msclock/univesal/compare/v1.0.9...v1.0.10) (2023-10-19)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.20 ([4227903](https://gitlab.com/msclock/univesal/commit/42279037800d993295ee7a51676c52df2b6aaaa9))

## [1.0.9](https://gitlab.com/msclock/univesal/compare/v1.0.8...v1.0.9) (2023-10-15)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.18 ([7452d09](https://gitlab.com/msclock/univesal/commit/7452d09c377458e17862d240d76fe53316ee149f))

## [1.0.8](https://gitlab.com/msclock/univesal/compare/v1.0.7...v1.0.8) (2023-10-09)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.15 ([fabf0be](https://gitlab.com/msclock/univesal/commit/fabf0be7933bf71c85e1c5d35870ec1c5f61ed79))


### Dependencies

* **deps:** update pre-commit hook igorshubovych/markdownlint-cli to v0.37.0 ([b3e39e9](https://gitlab.com/msclock/univesal/commit/b3e39e913f9d5808660e745977cef4eecba97939))
* **deps:** update pre-commit hook pre-commit/pre-commit-hooks to v4.5.0 ([33f403e](https://gitlab.com/msclock/univesal/commit/33f403e01847deb3e3accbca3a7fa5c26f2fdfe9))


### Chores

* enable pre-commit update based on renovate ([0c75e28](https://gitlab.com/msclock/univesal/commit/0c75e283a49eaece9e6d65f7bf9671cfccc69a6e))

## [1.0.7](https://gitlab.com/msclock/univesal/compare/v1.0.6...v1.0.7) (2023-10-05)


### Style

* indent 2 with json and js files ([8fd3f78](https://gitlab.com/msclock/univesal/commit/8fd3f78717bbd9639bf7ba64debbba518525b63d))


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.11 ([57ef049](https://gitlab.com/msclock/univesal/commit/57ef0494a4df76af0c320f6ba75b5c3f6001e691))


### Chores

* add vcpkg feature ([4d4ca54](https://gitlab.com/msclock/univesal/commit/4d4ca541cd39db04cbfdfb6cecc17833aaf79be3))
* use indent 2 for json files ([a7d26be](https://gitlab.com/msclock/univesal/commit/a7d26bea7120689660add94dcfe831492f948974))
